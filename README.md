# Presentación del Repositorio

Este repositorio hace parte de la asignatura "Redes Inalámbricas de Sensores" de la Universidad del Magdalena orientada por la Ing. Yesica Beltrán  Gómez

# Contiki

Contiki es un sistema operativo de código abierto para Internet de las Cosas. Permite conectar a Internet microcontroladores de bajo costo y bajo consumo energético.

Contiki está diseñado para sistemas embebidos con escasa memoria. Una configuración típica de Contiki consta de 2 KB de RAM y 40 KB de ROM. Cuenta con un núcleo orientado a eventos sobre el cual los programas pueden ser cargados y descargados de forma dinámica en tiempo de ejecución.

# Requerimientos del Curso

Tener el sistema operativo Linux (recomendación Ubuntu 18.04) o iOS. Si no cuenta con los sistemas operativos en mención en su computador, puede trabajar sobre máquina virtual. 

Máquina Virtual 

https://www.virtualbox.org/wiki/Downloads

ISO Ubuntu 18.04

https://releases.ubuntu.com/18.04/

Tenga en cuenta lo siguiente: 

1. Si instala Ubuntu desde la creación de la máquina virtual, virtualbox no le dará permisos de super usuario por lo que tendrá que modificar el archivo sudoers. 



# Adecuación del Entorno Virtual 

Para realizar la instación de Contiki, se deben tener unas condiciones previas en Linux, las cuales se muestran a continuación: 

1. Verificar si hay una versión de java instalada
```
java –version
```
Primera opción: No tener ninguna versión instalada, instalar la versión 8.
```
sudo apt install openjdk-8-jdk
```
Segunda opción: Tener la última versión de java (versión 11), configurar la versión 8.
```
sudo update-alternatives --config java
```
Escoger el número que corresponda a la versión 8.

2. Instalar Python
```
sudo apt-get install python2.7
```

3. Ejecute este comando en la terminal para que las URL apunten a los repositorios correctos (los repositorios más antiguos se mueven al servidor de archivos):
```
sudo sed -i -re 's/([a-z]{2}\.)?archive.ubuntu.com|security.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list
```
```
sudo gedit /etc/apt/sources.list
```
En el archivo que se abre debe reemplazar donde diga "old-releases" por "archive", guardar y cerrar.
```
sudo apt-get update
```
```
sudo apt-get upgrade
```

4. Instalar los siguientes paquetes

```
sudo apt-get install build-essential binutils-msp430 gcc-msp430 msp430-libc binutils-avr gcc-avr gdb-avr avr-libc avrdude binutils-arm-none-eabi gcc-arm-none-eabi openjdk-8-jdk openjdk-8-jre ant libncurses5-dev doxygen srecord git
```

# Instalar Contiki OS

Consideraciones a tener en cuenta para instalar contiki: 

1. Tener una cuenta de GitHub

2. Generar un token de autorización desde la cuenta de GitHub, para esto revise el siguiente tutorial

https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token

3. Verificar si tienen instalado Git, si no es así, ejecute el siguiente comando
```
sudo apt install git
```

4. Ejecutar los siguientes comandos para la instalación de Contiki
```
git clone --recursive git://github.com/contiki-os/contiki.git contiki
```
Si este enlace no funciona, utilizar el siguiente:
```
git clone --recursive https://github.com/contiki-os/contiki contiki

```
Escriba en la terminal el siguiente comando
```
cd contiki/tools
```
Escriba y ejecute "ls" y verifique que se encuentra la carpeta mspsim dentro del directorio
Ejecute el siguiente comando para eliminar la carpeta 
```
rm -rf mspsim
```
Clone la carpeta mspsim desde el repositorio GitHub
```
git clone --recursive https://github.com/contiki-os/mspsim mspsim 
```
Ejecute el siguiente comando
```
snap install ant --classic
```
Verifique que cuenta con las siguientes librerias y recursos
```
sudo apt-get install lib32ncurses5
```
```
sudo apt-get install gcc-msp430
```
Verifique la instalación con el siguiente comando
```
cd
cd contiki/examples/hello-world
make TARGET=native hello-world
./hello-world.native
```
Debe salir en pantalla un mensaje similar al siguiente:
```
Contiki-3.x-3345-g32b5b17f6 started with IPV6, RPL
Rime started with address 1.2.3.4.5.6.7.8
MAC nullmac RDC nullrdc NETWORK sicslowpan
Tentative link-local IPv6 address fe80:0000:0000:0000:0302:0304:0506:0708
Hello, world
```
Presione CTRL+C y ejecute la siguiente línea de comandos para iniciar el emulador
```
cd
cd contiki/tools/cooja
sudo ant run
```